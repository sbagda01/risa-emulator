#ifndef RISA_CORE_H
#define RISA_CORE_H

#include <defs.h>

void free_variables();
int find_mem_addr(char *,int offset);
int ask_memory(int tsize,int count);
void yy_scan_string(char *);
void yylex_destroy();
gboolean is_in_mem(Simulator *sim,Mem_index to);
void free_memory();
void init_mem_line(int);
int find_growing_factor(int num,int from,int limit);
gboolean is_power_two (int x);
gboolean is_stop_instr(Simulator *sim);
void stop_exec(Simulator *sim);
gboolean out_of_bounds(Simulator *s);
void swap_pc(Simulator*);
void reverse_exec_prev(Simulator *sim);
void reverse_exec_next(Simulator *sim);
void set_default_main_status(Simulator *sim);
void release_tok_mem_r(Simulator* sim);
gboolean single_instr(Simulator *s);
void  update_deb_console(Simulator *sim);
void set_up_sim(Simulator *sim);
void update_pc(short off);
short read_from_mem(int address);
void write_to_mem(int address,short value);
void memory_size_check(Mem_index index);
Mem_index memory_index(int address);
void init_memory();
void run(Simulator *);
void add_func(Instr_input input);
void add_func(Instr_input input);
void sub_func(Instr_input input);
void add1_func(Instr_input input);
void sub1_func(Instr_input input);
void neg_func(Instr_input input);
void xor_func(Instr_input input);
void xori_func(Instr_input input);
void mul2_func(Instr_input input);
void div2_func(Instr_input input);
void exch_func(Instr_input input);
void bgez_func(Instr_input input);
void blz_func(Instr_input input);
void bevn_func(Instr_input input);
void bodd_func(Instr_input input);
void bra_func(Instr_input input);
void rbra_func(Instr_input input);
void swbr_func(Instr_input input);
void data_func(Instr_input input);
void update_branch(short off);
void reverse(char s[]);
int init_pc_br_dir(Simulator  *sim);
char * str_for_reg(short i,short val);
void update_register_g(Simulator *sim,short index,short val);
void itoa_m(int n, char s[]);
void update_pc_g(Simulator *,Move_registers);
int  parse(Simulator * sim);
void clean_up_parser(Instruction**);

#endif
