#ifndef DEFS_H
#define DEFS_H

#define DEBUG_L1 
//#define DEBUG_L2

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkkeysyms.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <risa_errors.h>
#include <risa_limits.h>

#ifdef DEBUG_L2
#define PRINT_DEBUG_INFO(s) printf s
#else
#define PRINT_DEBUG_INFO(s) do{} while(0)
#endif


typedef struct
{
	char* name;
	int from;
	int to;
	int tsize;

}Var_memory;


typedef struct
{
	int from;
	int to;
}Range;


typedef struct
{
	int regd:BITS_FOR_REGISTERS;
	int regs:BITS_FOR_REGISTERS;
	int   off:BITS_FOR_OFF;
	int   imm:BITS_FOR_IMM;
	char *label;
	char *var_name;
	int count;
	int type;
}__attribute__ ((__packed__))Instr_input;

typedef void (*ins_ptr_t)(Instr_input);

typedef struct
{
	int current_size;
	short *data;

}Vector;


typedef struct
{
	int current_len;
	Vector **matrix;
}Memory;

typedef struct
{
	short REGISTERS[REGISTER_NUMBER];
	ins_ptr_t ins_ptr_array[INSTRUCTION_COUNT];
	Memory memory;
}Memory_hierch;


typedef struct 
{
	int program_counter;
	int prev_pc;
	int branch_register;
	char  direction;
}Move_registers;


typedef struct
{
	short line;
	short col;

}Mem_index;

enum instructions_id
{	
	add,
	sub,
	add1,
	sub1,
	nrg,
	xor,
	xori,
	mul2,
	div2,
	exch,
	bgez,
	blz,
	bevn,
	bodd,
	bra,
	rbra,
	swbr,
	data,	
	stop
};
	
enum types
{
	short_type,
	int_type

};

enum instruction_categories
{
	reg_reg,
	reg_noth,
	reg_imm,
	reg_off,
	noth_off,
	stop_cat
};

typedef struct
{
	GtkWidget		*text_view;	       
	GtkTextBuffer	*buffer;
	GtkWidget		*code_status;
	GtkSearchEntry	*search_entry;
	gchar *filename;		
}Code_editor;

typedef struct
{
	GtkLabel *pc;
	GtkLabel *br;
	GtkLabel *direction;	
}Program_counter_g;

typedef struct
{
	GtkEntry *registers_g[REGISTER_NUMBER];
	GtkTextView *info_terminal;
	GtkTextBuffer *buff;
	Program_counter_g pc_br_dir;	

}Registers_g;

typedef struct
{
	int category;
	int instr_id;;
	int actual_line;
	Instr_input input;;
}Instruction;


typedef struct {
	int err_count;
}Syntax_error;

typedef struct{
	Syntax_error ser;
}Error_manager;


typedef struct
{
	GtkTextView *info_terminal_mem;
	GtkTextView *show_mem;	
	GtkTextBuffer *mem_buff;
	GtkEntry *mem_from;
	GtkEntry *mem_to;
	GtkButton *apply;
}Memory_g;

typedef struct 
{
	Code_editor *editor;	
	Registers_g registers_tab;
	Memory_g	memory_tab;
	GtkWidget	*status_bar;
	GtkWidget	*window;
	GtkBuilder	*builder;
	GtkButton *execute;
	GtkButton *stop_btn;
	GtkCheckButton *debug;
	Instruction **instr;
	Move_registers mv;
	Memory_hierch storage;
	gboolean curr_debug;
	gboolean curr_exec;
	gboolean reversed_prev;
	gboolean reversed_next;
	Error_manager err_mgr;

}Simulator;

typedef struct
{
	char * string;
	int line_number;

}Label;

Instruction **instructions;
Label **labels;
Var_memory *variables_mem;


int instruction_count;
int lbl;
int label_count;
int internal_line;
int variables;
int variable_count;
int runtime_errors;

Simulator *simulator;
void append_to_console(char* ,Simulator *);

#endif
