#ifndef RISA_H
#define RISA_H
#include <defs.h>
/*  callbacks prototypes*/
void on_window_destroy (GObject *object, Simulator *sim);
void on_save_menu_item_activate(GtkMenuItem *menu_item,Simulator *sim);
void on_open_menu_item_activate(GtkMenuItem *menu_item, Simulator *sim);
void on_save_as_menu_item_activate(GtkMenuItem *menu_item, Simulator *sim);
void on_quit_menu_item_activate(GtkMenuItem *menu_item, Simulator *sim);
void on_next_btn_clicked(GtkWidget *w,Simulator *sim);
void on_stop_exec_btn_clicked(GtkWidget *w,Simulator *sim);
/* callbacks prototypes*/
void on_r_changed(GtkEditable *ed,Simulator *sim);
void on_window_destroy (GObject *object, Simulator *sim);
void on_save_menu_item_activate(GtkMenuItem *menu_item,Simulator *sim);
void on_open_menu_item_activate(GtkMenuItem *menu_item, Simulator *sim);
void on_save_as_menu_item_activate(GtkMenuItem *menu_item, Simulator *sim);
void on_quit_menu_item_activate(GtkMenuItem *menu_item, Simulator *sim);
void on_execute_btn_clicked(GtkWidget *w,Simulator *sim);
/* auxiliary functions prototypes */
int init_mem_tab(Simulator *sim);
int init_gui(Simulator *sim);
int init_editor(Simulator *sim);
void search_code_changed(GtkSearchEntry *entry,GtkTextBuffer *buffer);
void update_code_statusbar(GtkTextBuffer *buffer,GtkStatusbar  *statusbar);
static void mark_set_callback(GtkTextBuffer *buffer,const GtkTextIter *new_location,
																		GtkTextMark *mark,gpointer data);
gboolean check_for_save(Code_editor *editor);
void print_memory(Simulator *simul,int,int);
void debug_single_inst(Simulator *sim);
gchar *get_open_filename(Simulator *sim);
void set_main_status(Simulator *sim ,gchar *msg);
void load_file_into_buffer(Simulator *sim, gchar *filename);
gchar * get_save_filename(Simulator *sim);
void write_textv_to_file(Simulator *sim,gchar *filename);
void error_message(const gchar *message);
int init_reg_tab(Simulator *sim);
void clear_console(Simulator *sim);
void append_to_mem_buff(char *s,Simulator *sim);
const char *find_embedded_file(const char *file_name, size_t *size);

#endif
