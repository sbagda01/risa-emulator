#ifndef RISA_PARSER_H
#define RISA_PARSER_H

#include <defs.h>
int yylex();
void check_data_len();
void yyerror(const char *);
void risa_error(const char *,int, Simulator* );
int parsing_errors;
int reached_stop;
void new_outbranch(char* );
void new_inbranch(char* );
void new_fullbranch(char* ,char*);
Instruction *new_instruction(int category,char *name);
void free_labels();
void write_labels();
void release_tok_mem(Instruction **in);
void print_the_code();
int find_instr_id(char *);

/*
char *strsep_m(char **stringp,char* delim);
Instruction ** tokenize(char*,Simulator* sim);
int remove_cmnt_count_lines(char* str);
void clear_comment(char*);
Instruction *extract_instruction(const char * line,Label **,int ,int);
Instr_input find_instr_inputs(char* str,int);
int find_instr_id(char *);
int set_category (int id);
int in_label(char* label);
void strcpy_from(char * to,char* from,int start);
void syntax_error(int error_code,char *msg,int line);
*/

#endif
