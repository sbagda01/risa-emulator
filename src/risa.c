/*	
 *	Copyright (C) 2015 Sirgiy Bagdasar <sergio17949@gmail.com>
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 *
 **/

#include <risa_core.h>
#include <risa.h>

int main (int argc, char *argv[])
{
	Simulator *sim=NULL;
	sim=g_slice_new(Simulator);	
	gtk_init (&argc, &argv);
	if(init_gui(sim)) return 1;        
	gtk_widget_show (sim->window);		
	gtk_main ();
	free_memory();
	g_slice_free(Simulator,sim);
	return 0;
}
int init_gui(Simulator *sim)
{
	GError *err=NULL;
	sim->editor=g_slice_new(Code_editor);
	sim->builder = gtk_builder_new ();
	size_t gui_size;
	const char *GUI_STRING=find_embedded_file("risa_gui.xml",&gui_size);//we have already turned
	//gui .xml to c file and this will take it back. In this way we embed .xml to executable
	if(!gtk_builder_add_from_string(sim->builder,GUI_STRING,gui_size,&err))
	{
		error_message(err->message);
		return 1;
	
	}
	sim->window = GTK_WIDGET (gtk_builder_get_object (sim->builder, "window"));
	sim->status_bar=GTK_WIDGET(gtk_builder_get_object(sim->builder,"status_bar"));
	sim->execute=GTK_BUTTON(gtk_builder_get_object(sim->builder,"execute_btn"));
	sim->stop_btn=GTK_BUTTON(gtk_builder_get_object(sim->builder,"stop_exec_btn"));
	sim->debug=GTK_CHECK_BUTTON(gtk_builder_get_object(sim->builder,"debug_check"));
	gtk_builder_connect_signals (sim->builder,sim);          
	
	
	if(init_editor(sim)) return 1;
	if(init_reg_tab(sim)) return 1;
	if(init_mem_tab(sim)) return 1;
	
	set_default_main_status(sim);
	
	set_up_sim(sim);
	init_memory();
	
	g_object_unref(G_OBJECT(sim->builder));
	return 0;
}

int init_mem_tab(Simulator *sim)
{
	sim->memory_tab.info_terminal_mem=GTK_TEXT_VIEW(gtk_builder_get_object(sim->builder,
																			"info_terminal_mem"));
	gtk_text_view_set_buffer(sim->memory_tab.info_terminal_mem,sim->registers_tab.buff);
	sim->memory_tab.show_mem=GTK_TEXT_VIEW(gtk_builder_get_object(sim->builder,"show_memory"));
	sim->memory_tab.mem_buff=gtk_text_view_get_buffer(sim->memory_tab.show_mem);
	sim->memory_tab.apply=GTK_BUTTON(gtk_builder_get_object(sim->builder,"show_mem_btn"));
	sim->memory_tab.mem_from=GTK_ENTRY(gtk_builder_get_object(sim->builder,"from_mem"));
	sim->memory_tab.mem_to=GTK_ENTRY(gtk_builder_get_object(sim->builder,"to_mem"));
	gtk_text_buffer_create_tag(sim->memory_tab.mem_buff,"mem_font","font","Courier Bold 12",NULL);
	return 0;
	
}


int init_reg_tab(Simulator *sim)
{
	if(init_pc_br_dir(sim)) return 1;
	short i,j;
	char s[7],si[5];
	s[0]='r';	//get pointer to all register labels
	for(i=0;i<REGISTER_NUMBER;i++)
	{
		itoa_m(i,si);
		for(j=1;j<4;j++)
			s[j]=si[j-1];
		sim->registers_tab.registers_g[i]=GTK_ENTRY(gtk_builder_get_object(sim->builder,s));
	}
	sim->registers_tab.info_terminal=GTK_TEXT_VIEW(gtk_builder_get_object(sim->builder,"info_terminal"));
	sim->registers_tab.buff=gtk_text_view_get_buffer(sim->registers_tab.info_terminal);
	gtk_text_buffer_create_tag(sim->registers_tab.buff, "gray_bg", "background", "lightgray", NULL); 
	return 0;
}


void update_register_g(Simulator *sim,short index,short val)
{
	char *str=(char*)malloc(6); //6 is the length of short number
	itoa_m(val,str);
	gtk_entry_set_text(sim->registers_tab.registers_g[index],str);
	free(str);
}

void update_deb_console(Simulator *sim)
{
	if(out_of_bounds(sim)==TRUE)
		return;
	int pc=sim->instr[sim->mv.program_counter]->actual_line;
	int start,end,actual,actualn;
	GtkTextIter iter_start,iter_end,iter_actual,iter_actualn;
	gchar *text=NULL;
	if(pc==0)
	{
		start=pc;
		end=pc+2;
		actual=0;
		actualn=1;
	}
	else if(pc==instruction_count-1) 
	{
		start=pc-1;
		end=pc+1;	
		actual=1;
		actualn=2;
	}
	else
	{
		start=pc-1;
		end=pc+2;
		actual=1;
		actualn=2;
	}
	gtk_text_buffer_get_iter_at_line(sim->editor->buffer,&iter_start,start);
	gtk_text_buffer_get_iter_at_line(sim->editor->buffer,&iter_end,end);
	text=gtk_text_buffer_get_text(sim->editor->buffer,&iter_start,&iter_end,FALSE);
	  	
	gtk_widget_set_sensitive(GTK_WIDGET(sim->registers_tab.info_terminal),FALSE);
	gtk_text_buffer_set_text(sim->registers_tab.buff,text,-1);
  	
	gtk_text_buffer_get_iter_at_line(sim->registers_tab.buff,&iter_actual,actual);
	gtk_text_buffer_get_iter_at_line(sim->registers_tab.buff,&iter_actualn,actualn);
	gtk_text_buffer_apply_tag_by_name(sim->registers_tab.buff,"gray_bg",&iter_actual,&iter_actualn);
	
	
	gtk_text_buffer_set_modified(sim->registers_tab.buff,FALSE);
	gtk_widget_set_sensitive(GTK_WIDGET(sim->registers_tab.info_terminal),TRUE);
	
	g_free(text);
	
}


void update_pc_g(Simulator *sim,Move_registers mv)
{
	char str[16];
	itoa_m(mv.program_counter,str);
	gtk_label_set_text(sim->registers_tab.pc_br_dir.pc,str);
	str[0]='\0';
	itoa_m(mv.branch_register,str);
	gtk_label_set_text(sim->registers_tab.pc_br_dir.br,str);
	str[0]=mv.direction;
	str[1]='\0';
	gtk_label_set_text(sim->registers_tab.pc_br_dir.direction,str);
}
char * str_for_reg(short i,short val)
{
	char *s;
	s=(char*)malloc(20);
	short j=0;
	s[0]='R';
	s[1]='[';
	itoa_m(i,s+2);
	if(i>9)
		j=4;
	else
		j=3;
	s[j]=']';
	s[j+1]='=';	
	itoa_m(val,s+j+2);
	return s;
}
void itoa_m(int n, char s[])
{
	int i, sign;
	if ((sign = n) < 0)  /*  record sign */
		n = -n;          /*  make n positive */
	i = 0;
	do 
	{							/*  generate digits in reverse order */
		s[i++] = n % 10 + '0';   /*  get next digit */
	}while ((n /= 10) > 0);     /*  delete it */
	if (sign < 0)
		s[i++] = '-';
	s[i] = '\0';
	reverse(s);
}

/*  reverse:  reverse string s in place */
void reverse(char s[])
{
	int i, j;
	char c;
	for (i = 0, j = strlen(s)-1; i<j; i++, j--) 
	{
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
}

int init_pc_br_dir(Simulator  *sim)
{
	sim->registers_tab.pc_br_dir.pc=GTK_LABEL(gtk_builder_get_object(sim->builder,"pc_val_lbl"));
	sim->registers_tab.pc_br_dir.br=GTK_LABEL(gtk_builder_get_object(sim->builder,"branch_reg_val"));
	sim->registers_tab.pc_br_dir.direction=GTK_LABEL(gtk_builder_get_object(sim->builder,
																				"direction_val"));
	return 0;
}

int init_editor(Simulator *sim)
{
	sim->editor->text_view=GTK_WIDGET(gtk_builder_get_object(sim->builder,"text_view"));
	sim->editor->code_status=GTK_WIDGET(gtk_builder_get_object(sim->builder,"code_status"));
	sim->editor->buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(sim->editor->text_view));
	sim->editor->filename=NULL;
	gtk_text_buffer_create_tag(sim->editor->buffer, "gray_bg", "background", "lightgray", NULL); 
	sim->editor->search_entry=GTK_SEARCH_ENTRY(gtk_builder_get_object(sim->builder,\
																			"search_code"));
	g_signal_connect(sim->editor->buffer, "changed",G_CALLBACK(update_code_statusbar),\
														GTK_STATUSBAR(sim->editor->code_status));
	g_signal_connect(sim->editor->buffer, "mark_set",G_CALLBACK(mark_set_callback)\
													,GTK_STATUSBAR( sim->editor->code_status));
	g_signal_connect(sim->editor->search_entry,"search-changed",G_CALLBACK(search_code_changed)\
																	,sim->editor->buffer);
	update_code_statusbar(sim->editor->buffer, GTK_STATUSBAR(sim->editor->code_status));
	set_default_main_status(sim);
	return 0;
}
void on_window_destroy (GObject *object, Simulator *sim)
{
    if(check_for_save(sim->editor)==TRUE)
		on_save_menu_item_activate(NULL,sim);	
	gtk_main_quit();
}
void search_code_changed(GtkSearchEntry *entry,GtkTextBuffer *buffer)
{
	const gchar *text;
	GtkTextIter start_find, end_find;
	GtkTextIter start_match, end_match;
	text=gtk_entry_get_text(GTK_ENTRY(entry));
	gtk_text_buffer_get_start_iter(buffer, &start_find);
    gtk_text_buffer_get_end_iter(buffer, &end_find);
	gtk_text_buffer_remove_tag_by_name(buffer, "gray_bg", &start_find, &end_find);  
	while (gtk_text_iter_forward_search(&start_find, text, GTK_TEXT_SEARCH_TEXT_ONLY | 
						GTK_TEXT_SEARCH_VISIBLE_ONLY|GTK_TEXT_SEARCH_CASE_INSENSITIVE,
						 &start_match, &end_match, NULL)) 
	{
		gtk_text_buffer_apply_tag_by_name(buffer, "gray_bg", &start_match, &end_match);
		gint offset = gtk_text_iter_get_offset(&end_match);
		gtk_text_buffer_get_iter_at_offset(buffer, &start_find, offset);
	}
}

void update_code_statusbar(GtkTextBuffer *buffer,GtkStatusbar  *statusbar) 
{
	gchar *msg;
	gint row, col;
	GtkTextIter iter;
  
	gtk_statusbar_pop(GTK_STATUSBAR(statusbar), 0); 

	gtk_text_buffer_get_iter_at_mark(buffer,
      &iter, gtk_text_buffer_get_insert(buffer));

	row = gtk_text_iter_get_line(&iter);
	 col = gtk_text_iter_get_line_offset(&iter);

	msg = g_strdup_printf("Col: %d Ln: %d", col, row);

	gtk_statusbar_push(GTK_STATUSBAR(statusbar), 0, msg);

	g_free(msg);
}


static void mark_set_callback(GtkTextBuffer *buffer,const GtkTextIter *new_location,
																GtkTextMark *mark,gpointer data) 
{
	update_code_statusbar(buffer, GTK_STATUSBAR(data));
}


G_MODULE_EXPORT
void on_r_changed(GtkEditable *ed,Simulator *sim)
{
	if(sim->curr_debug==FALSE || sim->curr_exec==TRUE) return;
	const gchar *name;
	int index=0,number;
	name=gtk_buildable_get_name(GTK_BUILDABLE (ed));
	index=atoi(name+1);
	number=atoi(gtk_entry_get_text(GTK_ENTRY(ed)));
	sim->storage.REGISTERS[index]=number;	
	PRINT_DEBUG_INFO(("%d register value=%d\n",index,sim->storage.REGISTERS[index]));
}


G_MODULE_EXPORT
void on_execute_btn_clicked(GtkWidget *w,Simulator *sim)
{	
	
	//if debuging mode is running  or the still executes,exit
	if(sim->curr_debug==TRUE || sim->curr_exec==TRUE) 
		return;
	
	//wait for all gui events to complete
	while(gtk_events_pending()) gtk_main_iteration();
	
	//pasre the instructions
	if(parse(sim)==1) return;
	
	//set up new emulation
	set_up_sim(sim);	


	//enter to debug mode or execute on separate thread
	if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(sim->debug))==TRUE)
	{
		sim->curr_debug=TRUE;
		set_main_status(sim,"Status: Debugging");
		update_deb_console(sim);
	}
	else
	{
		clock_t begin, end;
		double time_spent;
		set_main_status(sim,"Status: Executing");
		sim->curr_exec=TRUE;
		//pthread_create(&(sim->th1),NULL,(void*)&run,(void*)(sim)); 
		
		//give choice to user thread or not
		if(TRUE)
		{	
			begin=clock();
			run(sim);
			end=clock();
			time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
			append_to_console(g_strdup_printf("Execution Time=%.4fs\n",time_spent),sim);
		}
	}
}

void print_memory(Simulator *simul,int from,int to)
{
	int i,j;
	char s[1024];
	gtk_text_buffer_set_text(simul->memory_tab.mem_buff,"",-1);
	if(from>to || from<0 || to<0 || to>SHRT_MAX) 
	{
		sprintf(s,"Error: Wrong memory range\n");
		append_to_console(s,simul);
		return;
	}
	Mem_index index_from=memory_index(from) ;
	Mem_index index_to=memory_index(to);
	if(is_in_mem(simul,index_to)==FALSE)
	{
		sprintf(s,"Error: Out of usable memory range\n");	
		append_to_console(s,simul);
		return;
	}
	sprintf(s,"Address\t\tValue\n");
	append_to_mem_buff(s,simul);
	s[0]='\0';
	for (i=index_from.line;i<=index_to.line;i++)
	{
		if(simul->storage.memory.matrix[i]!=NULL)
			for(j=index_from.col;j<=simul->storage.memory.matrix[i]->current_size;j++)
			{
				if(i==index_to.line && j>index_to.col)
				{
					break;
				}
				sprintf(s,"%d\t\t%d\n",i*MAX_MEM_WIDTH+j,simul->storage.memory.matrix[i]->data[j]);	
				append_to_mem_buff(s,simul);
				s[0]='\0';
			}
	}
	GtkTextIter iter_start;
	GtkTextIter iter_end;
	gtk_text_buffer_get_start_iter(simul->memory_tab.mem_buff,&iter_start);
	gtk_text_buffer_get_end_iter(simul->memory_tab.mem_buff,&iter_end);
	gtk_text_buffer_apply_tag_by_name(simul->memory_tab.mem_buff,"mem_font",&iter_start,&iter_end);

}

void append_to_mem_buff(char *s,Simulator *sim)
{
	GtkTextIter iter_end;
	gtk_text_buffer_get_end_iter (sim->memory_tab.mem_buff,&iter_end);	
	gtk_text_buffer_insert (sim->memory_tab.mem_buff,&iter_end,s,strlen(s));
		
}

void risa_error(char *s,int line,Simulator *sim)
{
	char *str;
	str=g_strdup_printf("Error near line %d, %s\n",line,s);
	append_to_console(str,sim);
	free(str);
}

void append_to_console(char *s,Simulator *sim)
{
	GtkTextIter iter_end;
	gtk_text_buffer_get_end_iter (sim->registers_tab.buff,&iter_end);	
	gtk_text_buffer_insert (sim->registers_tab.buff,&iter_end,s,strlen(s));
		
}

G_MODULE_EXPORT
void on_show_mem_clicked(GtkWidget *w,Simulator* sim)
{
	char *ptr,*ptr1;
	long from=strtol(gtk_entry_get_text(sim->memory_tab.mem_from),&ptr,10);			
	if(strlen(ptr)>0)
		return;
	long to=strtol(gtk_entry_get_text(sim->memory_tab.mem_to),&ptr1,10);			
	if(strlen(ptr1)>0)
		return;
	print_memory(sim,from,to);	
}

G_MODULE_EXPORT
void on_stop_exec_btn_clicked(GtkWidget *w,Simulator *sim)
{
	if(sim->curr_debug==FALSE) return;
	stop_exec(sim);
	gtk_text_buffer_set_text(sim->registers_tab.buff,"",-1);
}


void clear_console(Simulator *sim)
{
	gtk_text_buffer_set_text(sim->registers_tab.buff,"",-1);
}

G_MODULE_EXPORT
void on_next_btn_clicked(GtkWidget *w,Simulator *sim)
{
	if(sim->curr_debug==FALSE) return;
	reverse_exec_next(sim);	
	debug_single_inst(sim);
}

G_MODULE_EXPORT
void on_prev_btn_clicked(GtkWidget *w,Simulator *sim)
{
	if(sim->curr_debug==FALSE) return;
	reverse_exec_prev(sim);	
	debug_single_inst(sim);	
}

void debug_single_inst(Simulator *sim)
{
	gboolean err=single_instr(sim);
	if(err==FALSE)
	{
		stop_exec(sim);
		return;
	}
	update_deb_console(sim);
	if(is_stop_instr(sim))
	{
		stop_exec(sim);
		clear_console(sim);
	}
}

G_MODULE_EXPORT 
void on_open_menu_item_activate(GtkMenuItem *menu_item,Simulator *sim)
{
	gchar *filename;	
	if (check_for_save(sim->editor)==TRUE)
	{
		on_save_menu_item_activate(NULL,sim);
	}
	filename=get_open_filename(sim);
	if(filename!=NULL) load_file_into_buffer(sim,filename);	

}
G_MODULE_EXPORT 
void on_save_menu_item_activate(GtkMenuItem *menu_item,Simulator *sim)
{
	if(sim->editor->filename==NULL)
	{
		sim->editor->filename=get_save_filename(sim);
		if(sim->editor->filename!=NULL) 
			write_textv_to_file(sim,sim->editor->filename);
	}
	else write_textv_to_file(sim,sim->editor->filename);
	
}

gboolean check_for_save(Code_editor *editor)
{
	gboolean resp=FALSE;
	if(gtk_text_buffer_get_modified(editor->buffer) == TRUE)	
	{
		GtkWidget *dialog;
		const gchar *msg="Do you want to save the changes you have made?";
		dialog = gtk_message_dialog_new (NULL,GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
												GTK_MESSAGE_QUESTION,GTK_BUTTONS_YES_NO,"%s",msg);
		gtk_window_set_title(GTK_WINDOW(dialog),"Save?");
		if(gtk_dialog_run(GTK_DIALOG(dialog))!=GTK_RESPONSE_NO)
		{
			resp=TRUE;	
		}	
		gtk_widget_destroy(dialog);
	}
	return resp;
}

gchar *get_open_filename(Simulator *sim)
{
	GtkWidget *file_chooser;
	gchar *filename=NULL;
	file_chooser= gtk_file_chooser_dialog_new ("Open file...",GTK_WINDOW(sim->window),
														GTK_FILE_CHOOSER_ACTION_OPEN,
														"gtk-cancel", GTK_RESPONSE_CANCEL,
														"gtk-open", GTK_RESPONSE_OK,
														NULL);
    if(gtk_dialog_run(GTK_DIALOG(file_chooser))==GTK_RESPONSE_OK)
	{
		filename=gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_chooser));

	}
	gtk_widget_destroy(file_chooser);
	
	return filename;
}
void load_file_into_buffer(Simulator *sim,gchar *filename)
{
	GError *err=NULL;
	gchar *text=NULL;
	
	//GtkTextBuffer *buffer;
	/* add main statusbat message */
	/*ensure that all GUI events has happened until the next step*/
	
	while(gtk_events_pending()) gtk_main_iteration();
	
	if(!g_file_get_contents(filename,&text,NULL, &err))
	{
		/*error with file loading */
		error_message(err->message);
		g_error_free(err);
		g_free(filename);
	}
	
	/* disable text view sensetivity while loading file into buffer*/
	gtk_widget_set_sensitive(sim->editor->text_view,FALSE);
	//sim->editor->buffer=gtk_text_view_get_buffer(GTK_TEXT_VIEW(sim->editor->text_view));
	gtk_text_buffer_set_text(sim->editor->buffer,text,-1);
	gtk_text_buffer_set_modified(sim->editor->buffer,FALSE);
	gtk_widget_set_sensitive(sim->editor->text_view,TRUE);
	g_free(text);
	
	/* set the current filename since the load was succesfull */
	if(sim->editor->filename!=NULL) g_free(sim->editor->filename);
	sim->editor->filename=filename;
	/*clean loading status*/
	set_default_main_status(sim);
}

void set_main_status(Simulator *sim ,gchar *msg)
{
	gtk_statusbar_pop(GTK_STATUSBAR(sim->status_bar),0);
	gtk_statusbar_push(GTK_STATUSBAR(sim->status_bar),0,msg);	

}
void set_default_main_status(Simulator *sim)
{
	gchar *file;
	gchar *status;
	
	if(!sim->editor->filename)
	{	
		file=g_strdup("(UNTITLED)");
	}	
	else
	{
		file=g_path_get_basename(sim->editor->filename);
	}
	status=g_strdup_printf("File: %s",file);
	gtk_statusbar_pop(GTK_STATUSBAR(sim->status_bar),0);
	gtk_statusbar_push(GTK_STATUSBAR(sim->status_bar),0,status);
	g_free(status);
	g_free(file);	

}
gchar *get_save_filename(Simulator *sim)
{
	GtkWidget *file_chooser;
	gchar *filename=NULL;
	file_chooser = gtk_file_chooser_dialog_new ("Save File...",GTK_WINDOW (sim->window),
														GTK_FILE_CHOOSER_ACTION_SAVE,
														"gtk-cancel", GTK_RESPONSE_CANCEL,
														"gtk-save", GTK_RESPONSE_OK,
														NULL);
         
	if(gtk_dialog_run(GTK_DIALOG(file_chooser))==GTK_RESPONSE_OK)
		filename=gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_chooser));
	gtk_widget_destroy(file_chooser);
	return filename;	
}

void write_textv_to_file(Simulator *sim,gchar *filename)
{
	GError *err=NULL;
	gchar *text;
	GtkTextBuffer *buffer;
	GtkTextIter start,end;
	
	while(gtk_events_pending()) gtk_main_iteration();
	
	/* disable text view sensetivity and save content to buffer*/
	gtk_widget_set_sensitive(sim->editor->text_view,FALSE);
	buffer=gtk_text_view_get_buffer(GTK_TEXT_VIEW(sim->editor->text_view));
	gtk_text_buffer_get_start_iter(buffer,&start);
	gtk_text_buffer_get_end_iter(buffer,&end);
	text=gtk_text_buffer_get_text(buffer,&start,&end,FALSE);
	gtk_text_buffer_set_modified(buffer,FALSE);
	gtk_widget_set_sensitive(sim->editor->text_view,TRUE);
	/* set the file content to the text from the text view */
	if(filename!=NULL)
	{
		if(g_file_set_contents(filename,text,-1,&err) == FALSE)
		{	
			error_message(err->message);
			g_error_free(err);
		}	
	}
	//g_free(text);
	set_default_main_status(sim);

}

void error_message(const gchar *message)
{
	GtkWidget *dialog;
	/* log to terminal windo */
	g_warning("%s",message);
	dialog = gtk_message_dialog_new (NULL,GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
										GTK_MESSAGE_ERROR,GTK_BUTTONS_OK,"%s",message);
    gtk_window_set_title (GTK_WINDOW (dialog), "Error!");
    gtk_dialog_run (GTK_DIALOG (dialog));      
    gtk_widget_destroy (dialog);         
}

G_MODULE_EXPORT
void on_save_as_menu_item_activate(GtkMenuItem *menu_item, Simulator *sim)
{
	gchar *filename;
	filename=get_save_filename(sim);
	if(filename!=NULL) write_textv_to_file(sim,filename);
}

G_MODULE_EXPORT
void on_quit_menu_item_activate(GtkMenuItem *menu_item, Simulator *sim)
{
	if(check_for_save(sim->editor)==TRUE)
		on_save_menu_item_activate(NULL,sim);
	gtk_main_quit();
}

G_MODULE_EXPORT
void on_cut_menu_item_activate(GtkMenuItem *menuitem, Simulator *sim)
{
	GtkTextBuffer           *buffer;
    GtkClipboard            *clipboard;
    clipboard = gtk_clipboard_get (GDK_SELECTION_CLIPBOARD);
    buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (sim->editor->text_view));
    gtk_text_buffer_cut_clipboard (buffer, clipboard, TRUE);
}

G_MODULE_EXPORT
void on_copy_menu_item_activate(GtkMenuItem *menuitem, Simulator *sim)
{
	GtkTextBuffer           *buffer;
    GtkClipboard            *clipboard;
        
    clipboard = gtk_clipboard_get (GDK_SELECTION_CLIPBOARD);
    buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (sim->editor->text_view));
    gtk_text_buffer_copy_clipboard (buffer, clipboard);
}

G_MODULE_EXPORT
void on_paste_menu_item_activate(GtkMenuItem *menuitem, Simulator *sim)
{
	GtkTextBuffer           *buffer;
    GtkClipboard            *clipboard;
   	clipboard = gtk_clipboard_get (GDK_SELECTION_CLIPBOARD);
	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (sim->editor->text_view));
    gtk_text_buffer_paste_clipboard (buffer, clipboard, NULL, TRUE);

}

G_MODULE_EXPORT
void on_about_menu_item_activate (GtkMenuItem *menuitem,Simulator *sim)
{
    static const gchar * const authors[] = {"Sergiy Bagdasar <sergio17949@gmail.com>",NULL};

	static const gchar copyright[] = "Copyright \xc2\xa9 2016 Sergiy Bagdasar";

	static const gchar comments[] = "Reversible Instruction Set Architecture Emulator";

	gtk_show_about_dialog (GTK_WINDOW (sim->window),
													"authors", authors,
													"comments", comments,
													"copyright", copyright,
													"version", "1.0",
													"program-name", "RISA Emulator",
													"logo-icon-name", "gtk-edit",NULL); 

}
G_MODULE_EXPORT
void on_new_menu_item_activate (GtkMenuItem *menuitem,Simulator *sim)
{
        GtkTextBuffer           *buffer;
        
        if (check_for_save (sim->editor) == TRUE)
        {
              on_save_menu_item_activate (NULL,sim);  
        }
        
        /* clear editor for a new file */
        sim->editor->filename = NULL;
        buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (sim->editor->text_view));
        gtk_text_buffer_set_text (buffer, "", -1);
        gtk_text_buffer_set_modified (buffer, FALSE);
        
        set_default_main_status (sim);
}




