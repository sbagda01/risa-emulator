#include <risa_parser.h>
#include <risa_core.h>

#include "rules.tab.h"
ins_ptr_t ins_ptr_array[INSTRUCTION_COUNT]=	{
												add_func,sub_func,add1_func,sub1_func,neg_func,
												xor_func,xori_func,mul2_func,div2_func,exch_func,
												bgez_func,blz_func,bevn_func,bodd_func,bra_func,
												rbra_func,swbr_func,data_func
											};	
Simulator* simul;


void  set_up_sim(Simulator *sim)
{
	simul=sim;	
	//init gui registers
	sim->mv.program_counter=0;
	sim->mv.branch_register=0;
	sim->mv.direction='F';
	sim->reversed_next=TRUE;
	sim->reversed_prev=FALSE;	
	update_pc_g(sim,sim->mv);	
	//init registers to zero
	int i;
	for(i=0;i<REGISTER_NUMBER;i++)
	{
		update_register_g(sim,i,0);
		sim->storage.REGISTERS[i]=0;
	}
	
}

int parse(Simulator *sim)
{	
	GtkTextIter start,end;
	gchar *text=NULL;
	
	gtk_text_buffer_set_text(sim->registers_tab.buff,"",-1);
	//parse instructions
	gtk_text_buffer_get_start_iter(sim->editor->buffer,&start);
	gtk_text_buffer_get_end_iter(sim->editor->buffer,&end);
	text=gtk_text_buffer_get_text(sim->editor->buffer,&start,&end,FALSE);
	
	instruction_count=INSTRUCTION_NUMBER;
	label_count=INSTRUCTION_NUMBER;
	variable_count=INSTRUCTION_NUMBER;

	lbl=0;
	parsing_errors=0;
	internal_line=0;
	variables=0;
	runtime_errors=0;
	simulator=sim;
	
	instructions=(Instruction **)malloc(sizeof(Instruction *)*instruction_count);
	if(instructions==NULL)
	{
		printf("Cant Allocate Memory For Instructions\n");
		return 1;
	}
	labels=(Label **)malloc(sizeof(Label*)*label_count); 
	if(labels==NULL)
	{	
		printf("Cant Allocate Memory For Labels\n");
		return 1;
	}
	variables_mem=(Var_memory* )malloc(variable_count*sizeof(Var_memory));
	if(variables_mem==NULL)
	{
		printf("Cant Allocate Memory For Variables");
		return 1;
	
	}
	variables_mem[0].to=0;
	variables_mem[0].from=0;

	yy_scan_string(text);
	yyparse();
	yylex_destroy();	
	
	if(parsing_errors>0) 
	{
		release_tok_mem(instructions);
		free_labels();	
		return 1;	
	}
	if(instructions==NULL) return 1;
	sim->instr=instructions;
	
	g_free(text);
	return 0;
}

void reverse_exec_prev(Simulator *sim)
{	
	if(sim->reversed_next==TRUE) 
		sim->reversed_next=FALSE;
	
	if(sim->reversed_prev==TRUE) return;
	if(sim->mv.direction=='F')
		sim->mv.direction='R';
	else
		sim->mv.direction='F';
	swap_pc(sim);
	update_pc_g(sim,sim->mv);
	sim->reversed_prev=TRUE;
}

void reverse_exec_next(Simulator *sim)
{
	if(sim->reversed_prev==TRUE) 
		sim->reversed_prev=FALSE;
	
	if(sim->reversed_next==TRUE) return;
	if(sim->mv.direction=='F')
		sim->mv.direction='R';
	else
		sim->mv.direction='F';
	swap_pc(sim);
	update_pc_g(sim,sim->mv);
	sim->reversed_next=TRUE;
}

void swap_pc(Simulator *sim)
{
	int t;
	t=sim->mv.program_counter;
	sim->mv.program_counter=sim->mv.prev_pc;
	sim->mv.prev_pc=t;

}

void release_tok_mem_r(Simulator* sim)
{
	release_tok_mem(sim->instr);
	free_variables();
}


void free_variables()
{
	int i;
	if(variables_mem==NULL) return;
	for(i=0;i<variables;i++)
	{
		if(variables_mem[i].name!=NULL)
		{	
			free(variables_mem[i].name);	
			variables_mem[i].name=NULL;
		}
	}
	variables=0;
	free(variables_mem);
	variables_mem=NULL;
}

void run(Simulator *sim) 
{	
	while(is_stop_instr(sim)==FALSE)
	{
		if(single_instr(sim)==FALSE)
			break;
	}
	stop_exec(sim);
}

void stop_exec(Simulator *sim)
{
	release_tok_mem_r(sim);
	sim->curr_debug=FALSE;
	sim->curr_exec=FALSE;
	set_default_main_status(sim);
}

gboolean single_instr(Simulator *s)
{	
	if(runtime_errors>0) return FALSE;
	char str[1024];
	if(out_of_bounds(s)==TRUE)
	{
		sprintf(str,"Out of program bounds no line %d\n",s->mv.program_counter);
		append_to_console(str,s);	
		return FALSE;
	}
	ins_ptr_array[s->instr[s->mv.program_counter]->instr_id](s->instr[s->mv.program_counter]->input);	
	return TRUE;
}

gboolean out_of_bounds(Simulator *s)
{
	if(s->mv.program_counter>=instruction_count || s->mv.program_counter<0)
		return TRUE;
	return FALSE;
}

gboolean is_stop_instr(Simulator *sim)
{
	if(out_of_bounds(sim)==TRUE) return FALSE;
	if(sim->instr[sim->mv.program_counter]->category==stop_cat)
		return TRUE;
	return FALSE;
}

void init_memory()
{
	simul->storage.memory.matrix=(Vector **)calloc(MEM_CHUNK_HEIGHT,sizeof(Vector*));
	if(simul->storage.memory.matrix==NULL)
	{
		perror("Cannot allocate memory");
		exit(1);
	}
	simul->storage.memory.current_len=MEM_CHUNK_HEIGHT;
}

void init_mem_line (int line)
{
	Vector *v =NULL;
	v=(Vector *)malloc(sizeof(Vector));
	if(v==NULL)
	{
		perror("Cannot allocate memory");
		//clean_up();
		exit(1);
	}
	v->current_size=MEM_CHUNK_WIDTH;
	v->data=(short*)malloc(sizeof(short)*MEM_CHUNK_WIDTH);
	if(v->data==NULL)
	{
		perror("Cannot allocate memory");
		//clean_up();
		exit(1);
	}
	simul->storage.memory.matrix[line]=v;
}

Mem_index memory_index(int address)
{
	Mem_index index;
	index.line=(int)(address)/MAX_MEM_WIDTH;
	index.col=address % MAX_MEM_WIDTH;		
	PRINT_DEBUG_INFO(("Memory index, line: %d,col %d\n",index.line,index.col));
	return index;
}

void memory_size_check(Mem_index index)
{
	char msg[1024];
	int curr_lines=simul->storage.memory.current_len;
	if(index.line>curr_lines)
	{
		
		int GROWING_FACTOR=find_growing_factor(index.line,curr_lines,MAX_MEM_HEIGHT);
		int i;
		PRINT_DEBUG_INFO(("Memory heigh is %d, growing by factor %d\n",curr_lines,GROWING_FACTOR));
		//grow the current memory length by groiwing factor
		if(GROWING_FACTOR>0)
		{
			simul->storage.memory.matrix=(Vector**)realloc
			(
			 simul->storage.memory.matrix,sizeof(Vector*)*curr_lines*GROWING_FACTOR
			);
			simul->storage.memory.current_len*=GROWING_FACTOR;
			curr_lines*=GROWING_FACTOR;
			for(i=curr_lines/GROWING_FACTOR;i<=curr_lines;i++)
				simul->storage.memory.matrix[i]=NULL;
			if(simul->storage.memory.matrix==NULL)
			{
				perror("Cannot allocate new memory lines\n");
				exit(1);	
			}
		}
		else
		{
			//cancel the execution , out of memory bounds
			sprintf(msg,"Error:Segmentation Fault no memory address: %d\n",
																index.line*MAX_MEM_WIDTH+index.col);
			append_to_console(msg,simul);
			stop_exec(simul);
		}
	}
	if(simul->storage.memory.matrix[index.line]==NULL)
		init_mem_line(index.line);
	int curr_cols=simul->storage.memory.matrix[index.line]->current_size;
	if(index.col>curr_cols)	
	{
		int GROWING_FACTOR=find_growing_factor(index.col,curr_cols,MAX_MEM_WIDTH);
		PRINT_DEBUG_INFO(("Memory width is %d, growing by factor %d\n",curr_cols,GROWING_FACTOR));
		
		if(GROWING_FACTOR>0)
		{	
			simul->storage.memory.matrix[index.line]->data=(short*)realloc
			(
			 simul->storage.memory.matrix[index.line]->data,curr_cols*GROWING_FACTOR*sizeof(short)
			);
			simul->storage.memory.matrix[index.line]->current_size*=GROWING_FACTOR;
			curr_cols*=GROWING_FACTOR;
		}
		else
		{
			//cancel the execution , out of memory bounds
			sprintf(msg,"Error:Segmentation Fault no memory address: %d\n",
																index.line*MAX_MEM_WIDTH+index.col);
			append_to_console(msg,simul);
			stop_exec(simul);
			//cancel the execution, out of memory bounds
		}
	}
	PRINT_DEBUG_INFO(("Current memory size: rows: %d, cols: %d\n",curr_lines,curr_cols));	
}
gboolean is_in_mem(Simulator *sim,Mem_index to)
{
	if(to.line>sim->storage.memory.current_len)	
		return FALSE;
	return TRUE;
}

void free_memory()
{
	int i;
	for(i=0;i<simul->storage.memory.current_len;i++)
	{	
		if(simul->storage.memory.matrix[i]!=NULL)
			free(simul->storage.memory.matrix[i]);
	}
	free(simul->storage.memory.matrix);
}

int find_growing_factor(int num,int from,int limit)
{
	int i;
	for(i=num;i<=limit;i++)
	{
		if(is_power_two(i))
			return i/from;			
	
	}
	return -1;

}

/*  Function to check if x is power of 2*/
gboolean is_power_two (int x)
{
	  /*  First x in the below expression is for the case when x is 0 */
     return x && (!(x&(x-1)));
}	 

void write_to_mem(int address,short value)
{
	Mem_index in;
	in=memory_index(address);
	memory_size_check(in);	
	simul->storage.memory.matrix[in.line]->data[in.col]=value;
}

short read_from_mem(int address)
{
	short d=0;
	Mem_index in;
	in=memory_index(address);
	memory_size_check(in);	
	d=simul->storage.memory.matrix[in.line]->data[in.col];
	return d;
}

int ask_memory(int tsize,int count)
{
	int i;
	int address;
	Mem_index in;
	int previous;
	if(variables==0)
		previous=0;
	else
		previous=variables-1;
	
	for(i=1;i<=count+1;i++)
	{
		//check all the addresses that will be allocated
		address=variables_mem[previous].to+i;
		in=memory_index(address);
		memory_size_check(in);
	}
	address=variables_mem[previous].to+1;
	return address; 
}

Range find_variable_address(char* name)
{
	Range range;
	range.to=-1;
	range.from=-1;
	int i;
	for(i=0;i<variables;i++)
	{
		if(strcmp(name,variables_mem[i].name)==0)
		{
			range.from=variables_mem[i].from;
			range.to=variables_mem[i].to;
			break;
		}
	}
	return range;	
}


void allocate_variable(char * name,int type,int count)
{
	Range range;
	range=find_variable_address(name);	
	if(range.from!=-1)
	{
			risa_error("Redeclaration of variable",
							instructions[simul->mv.program_counter]->actual_line,simul);	
			runtime_errors++;
			return;	
	}
	if(variables==(variable_count-1))
	{
		variable_count*=2;
		variables_mem=(Var_memory*)realloc(variables_mem,sizeof(Var_memory)*variable_count);
		if(variables_mem==NULL)
		{
			printf("Cant Reallocate Memory for Variables\n");
			exit(1);
		}
	}
	variables_mem[variables].name=name;
	int tsize;
	switch(type)
	{
		case short_type:	tsize=2;
							break;
	}

	variables_mem[variables].tsize=tsize;
	//for now memory is 2 byte alligned 
	variables_mem[variables].from=ask_memory(tsize,count);
	variables_mem[variables].to=variables_mem[variables].from+count;	
	variables++;
}

void update_pc(short off)
{
	simul->mv.prev_pc=simul->mv.program_counter;
	if(simul->mv.branch_register!=0) 
	{	
		if(simul->mv.direction=='F')
		{	
			simul->mv.program_counter+=simul->mv.branch_register;
		}
		else
		{
			simul->mv.program_counter-=(simul->mv.branch_register);
		}
		update_pc_g(simul,simul->mv);
		return;
	}
	if(simul->mv.direction=='F')
		simul->mv.program_counter+=off;
	else
		simul->mv.program_counter-=off;
	update_pc_g(simul,simul->mv);
}


void add_func(Instr_input input)
{
	if(simul->mv.direction=='F')
		simul->storage.REGISTERS[input.regd]+=simul->storage.REGISTERS[input.regs];
	else
		simul->storage.REGISTERS[input.regd]-=simul->storage.REGISTERS[input.regs];
	update_register_g(simul,input.regd,simul->storage.REGISTERS[input.regd]);
	update_pc(1);
}

void sub_func(Instr_input input)
{
	if(simul->mv.direction=='F')
		simul->storage.REGISTERS[input.regd]-=simul->storage.REGISTERS[input.regs];
	else
		simul->storage.REGISTERS[input.regd]+=simul->storage.REGISTERS[input.regs];
	update_register_g(simul,input.regd,simul->storage.REGISTERS[input.regd]);
	update_pc(1);
}

void add1_func(Instr_input input)
{
	if(simul->mv.direction=='F')
		simul->storage.REGISTERS[input.regd]+=1;
	else
		simul->storage.REGISTERS[input.regd]-=1;
	update_register_g(simul,input.regd,simul->storage.REGISTERS[input.regd]);
	update_pc(1);
}

void sub1_func(Instr_input input)
{	
	if(simul->mv.direction=='F')
		simul->storage.REGISTERS[input.regd]-=1;
	else
		simul->storage.REGISTERS[input.regd]+=1;
	update_register_g(simul,input.regd,simul->storage.REGISTERS[input.regd]);
	update_pc(1);
}
void neg_func(Instr_input input)
{
	simul->storage.REGISTERS[input.regd]=-simul->storage.REGISTERS[input.regd];
	update_register_g(simul,input.regd,simul->storage.REGISTERS[input.regd]);
	update_pc(1);
}

void xor_func(Instr_input input)
{
	short x=simul->storage.REGISTERS[input.regd];
	short y=simul->storage.REGISTERS[input.regs];
	//formula for xor calculation
	short a = x & y;
	short b = ~x & ~y;
	short z = ~a & ~b;
	simul->storage.REGISTERS[input.regd]=z;
	update_register_g(simul,input.regd,simul->storage.REGISTERS[input.regd]);
	update_pc(1);
}

void xori_func(Instr_input input)
{
	short x=simul->storage.REGISTERS[input.regd];
	short y=input.imm;
	//formula for xor calculation
	short a = x & y;
	short b = ~x & ~y;
	short z = ~a & ~b;
	simul->storage.REGISTERS[input.regd]=z;
	update_register_g(simul,input.regd,simul->storage.REGISTERS[input.regd]);
	update_pc(1);
}

void mul2_func(Instr_input input)
{
	if(simul->mv.direction=='F')
		simul->storage.REGISTERS[input.regd]*=2;	
	else
		simul->storage.REGISTERS[input.regd]/=2;		
	update_register_g(simul,input.regd,simul->storage.REGISTERS[input.regd]);
	update_pc(1);
}
void div2_func(Instr_input input)
{	
	if(simul->mv.direction=='F')
		simul->storage.REGISTERS[input.regd]/=2;	
	else
		simul->storage.REGISTERS[input.regd]*=2;		
	update_register_g(simul,input.regd,simul->storage.REGISTERS[input.regd]);
	update_pc(1);
}
	
void exch_func(Instr_input input)
{
	short d=simul->storage.REGISTERS[input.regd];
	Range range;
	int base;
	int target;
	if(input.var_name==NULL)
	{	
		//exchange with address in register
		simul->storage.REGISTERS[input.regd]=read_from_mem(simul->storage.REGISTERS[input.regs]);
		write_to_mem(simul->storage.REGISTERS[input.regs],d);
	}
	else 
	{
		//exchange with variable in memory
		range=find_variable_address(input.var_name);	
		if(range.from==-1)
		{
			runtime_errors++;
			risa_error("Unexistent Address",instructions[simul->mv.program_counter]->actual_line,simul);
			return;	
		}
		base=range.from;
		target=base+simul->storage.REGISTERS[input.off];
		if(target>range.to)
		{
			runtime_errors++;
			risa_error("Segmentation Fault",instructions[simul->mv.program_counter]->actual_line,simul);
			return;
		}
		simul->storage.REGISTERS[input.regd]=read_from_mem(target);
		write_to_mem(target,d);
	}
	
	//update register on gui
	update_register_g(simul,input.regd,simul->storage.REGISTERS[input.regd]);
	update_pc(1);

}
void update_branch(short off)
{
	if(simul->mv.direction=='F')
		simul->mv.branch_register+=off;	
	else
		simul->mv.branch_register-=off;
	update_pc(1);
}

void bgez_func(Instr_input input)
{
	if(simul->storage.REGISTERS[input.regd]>=0)
	{	
		update_branch(input.off);
	}
	else
		update_pc(1);	
}
void blz_func(Instr_input input)
{	
	if(simul->storage.REGISTERS[input.regd]<0)
		update_branch(input.off);
	else
		update_pc(1);	
}
	
void bevn_func(Instr_input input)
{
	if(simul->storage.REGISTERS[input.regd]%2==0)
		update_branch(input.off);
	else
		update_pc(1);	
}
void bodd_func(Instr_input input)
{
	if(simul->storage.REGISTERS[input.regd]%2==1)
		update_branch(input.off);
	else
		update_pc(1);	
}

void bra_func(Instr_input input)
{
	update_branch(input.off);
}

void rbra_func(Instr_input input)
{

	if(simul->mv.direction=='F')
	{
		simul->mv.branch_register=-simul->mv.branch_register-input.off;
		simul->mv.direction='R';	
	}
	else
	{
		simul->mv.branch_register=-simul->mv.branch_register+input.off;
		simul->mv.direction='F';
	}
	update_pc(1);	
}

void swbr_func(Instr_input input)
{
	short d;
	d=simul->storage.REGISTERS[input.regd];
	simul->storage.REGISTERS[input.regd]=simul->mv.branch_register;
	simul->mv.branch_register=d;
	update_register_g(simul,input.regd,simul->storage.REGISTERS[input.regd]);
	update_pc(1);
}

void data_func(Instr_input input)
{
	if(simul->mv.direction=='F')
		allocate_variable(input.var_name,input.type,input.count);
	else
	{
		variables--;
		free(variables_mem[variables].name);
	}
	update_pc(1);
}

