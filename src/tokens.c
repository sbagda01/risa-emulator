%{
	#include "rules.tab.h"
	#include <defs.h>
%}

%option nounput
%option noinput
%option yylineno

%%

"add" {
	PRINT_DEBUG_INFO(("found add\n"));
	yylval.instr=strdup(yytext);	
	return ADD;
}

"sub" {
	PRINT_DEBUG_INFO(("found sub\n"));
	yylval.instr=strdup(yytext);	
	return SUB;
}

"add1" {
	PRINT_DEBUG_INFO(("found add1\n"));
	yylval.instr=strdup(yytext);	
	return ADD1;
}

"sub1" {
	PRINT_DEBUG_INFO(("found sub1\n"));
	yylval.instr=strdup(yytext);	
	return SUB1;
}

"neg" {
	PRINT_DEBUG_INFO(("found neg\n"));
	yylval.instr=strdup(yytext);
	return NEG;
}

"xor" {
	PRINT_DEBUG_INFO(("found xor\n"));
	yylval.instr=strdup(yytext);	
	return XOR;
}

"xori" {
	PRINT_DEBUG_INFO(("found xori\n"));
	yylval.instr=strdup(yytext);	
	return XORI;
}

"mul2" {
	PRINT_DEBUG_INFO(("found mul2\n"));
	yylval.instr=strdup(yytext);
	return MUL2;
}

"div2" {
	PRINT_DEBUG_INFO(("found div2\n"));
	yylval.instr=strdup(yytext);
	return DIV2;
}

"exch" {
	PRINT_DEBUG_INFO(("found exch\n"));
	yylval.instr=strdup(yytext);
	return EXCH;

}

"bgez" {
	PRINT_DEBUG_INFO(("found bgez\n"));
	yylval.instr=strdup(yytext);
	return BGEZ;
}

"blz" {
	PRINT_DEBUG_INFO(("found blz\n"));
	yylval.instr=strdup(yytext);
	return BLZ;
}

"bevn" {
	PRINT_DEBUG_INFO(("found bevn\n"));
	yylval.instr=strdup(yytext);
	return BEVN;
}

"bodd" {
	PRINT_DEBUG_INFO(("found bodd\n"));
	yylval.instr=strdup(yytext);
	return BODD;
}

"bra" {
	PRINT_DEBUG_INFO(("found bra\n"));
	yylval.instr=strdup(yytext);
	return BRA;
}

"rbra" {
	PRINT_DEBUG_INFO(("found rbra\n"));
	yylval.instr=strdup(yytext);
	return RBRA;
}

"swbr" {
	PRINT_DEBUG_INFO(("found swbr\n"));
	yylval.instr=strdup(yytext);
	return SWBR;
}

"stop" {	
	
	PRINT_DEBUG_INFO(("found stop\n"));
	yylval.instr=strdup(yytext);	
	return STOP;
}

"data"	{

	PRINT_DEBUG_INFO(("found data\n"));
	yylval.instr=strdup(yytext);	
	return DATA;

}

"short" {
	PRINT_DEBUG_INFO(("found short\n"));
	yylval.instr=strdup(yytext);
	return SHORT_TYPE;
}

[$] {
	PRINT_DEBUG_INFO(("found character %c\n",yytext[0]));
	return yytext[0];
}

; { 
	PRINT_DEBUG_INFO(("HELLO found semicolon \n"));
	return yytext[0];
}

-?[0-9]+ {
		int n=atoi(yytext);
		PRINT_DEBUG_INFO(("found number %d\n",n));
		yylval.reg_number=n;
		return NUMBER;
}

([a-zA-Z]+[0-9]*)+ {
		PRINT_DEBUG_INFO(("found label %s\n",yytext));
		yylval.str=strdup(yytext);
		return ID;
}


[ \t\r\n]+ { PRINT_DEBUG_INFO(("eat some spaces \n"));}

#.* { PRINT_DEBUG_INFO(("eat some comments\n")); }

> {
	PRINT_DEBUG_INFO(("found %c\n",yytext[0]));
	return yytext[0];
}

[,:] {
	PRINT_DEBUG_INFO(("found %c\n",yytext[0]));
	return yytext[0];

}


. {
	PRINT_DEBUG_INFO(("ERROR: unexpected character %c\n",yytext[0]));
	return yytext[0];
}

%%

/*
	Called at EOF; (boolean) result = terminate or not
	LEX deals with EOF (e.g. control-D) as if with the following rule:
	EOF if (yywrap( )) return 0;
*/
int yywrap ()
{
	return 1;
}



