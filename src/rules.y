%{
	#include <stdio.h>
	#include <risa_parser.h>
	extern char *yytext;
	extern int yylineno;
	const char* instructions_strings[]=
	{
		"add","sub","add1","sub1","neg","xor","xori","mul2","div2",
		"exch","bgez","blz","bevn","bodd","bra","rbra","swbr","data",
		"stop"
	};
%}

%union 
{
	char* instr; 
	int reg_number;
	char* str;
}


%start program
%token <reg_number> NUMBER
%token <instr> ADD SUB ADD1 SUB1 NEG XOR XORI MUL2 DIV2 EXCH BGEZ BLZ BEVN BODD BRA RBRA SWBR STOP
%token <instr> DATA SHORT_TYPE
%token <str> ID
%type <instr> branch ubranch product reg_reg reg_noth variable
%type <reg_number> register immed type
%nonassoc LOWER_THAN_PAR
%nonassoc '('
%nonassoc NUMBER
%nonassoc LOWER_THAN_NUMBER

%%



program				: declarations instruction_list
					;


declarations		: declaration ';' declarations
					| %empty
					;

declaration			: DATA variable ',' type ',' NUMBER 	
					{
						instructions[internal_line]=new_instruction(data,$1);		
						instructions[internal_line]->input.var_name=$2;
						instructions[internal_line]->input.type=$4;
						instructions[internal_line]->input.count=$6;
						internal_line++;		
					
					}
					| DATA variable ',' type  %prec LOWER_THAN_NUMBER
					{
						instructions[internal_line]=new_instruction(data,$1);		
						instructions[internal_line]->input.var_name=$2;
						instructions[internal_line]->input.type=$4;
						instructions[internal_line]->input.count=0;
						internal_line++;		

					}
					;
					
variable			: ID {$$=$1;}
					; 

type				: SHORT_TYPE {$$=short_type;}
					;

instruction_list	: { check_data_len();} stmt ';' instruction_list 
					| label2 instruction_list
					| STOP 
					{
						instructions[internal_line]=new_instruction(stop_cat,$1);
						internal_line++;
						instruction_count=internal_line;
						write_labels();
#ifdef DEBUG_L1						
						print_the_code();
#endif
					}		 
					;

stmt				: reg_reg_stmt 
					| reg_noth_stmt
					| xori_stmt
					| ubranches_stmt 
					| exch_stmt 
					| branches_stmt
					| product_stmt
					| swbr_stmt 
					| error  { yyerror("Undefined Instruction");}
					;

reg_reg_stmt		: reg_reg  '$' register '$' register 
					{
						PRINT_DEBUG_INFO(("bison %s stmt\n",$1));
						instructions[internal_line]=new_instruction(reg_reg,$1);
						instructions[internal_line]->input.regd=$3;
						instructions[internal_line]->input.regs=$5;
						internal_line++;
					}
					| reg_reg '$' register error {yyerror("Missing Operand");} 
					| reg_reg error  {yyerror("Missing Operand");}
					| reg_reg immed error {yyerror("Unexpected Input,Expected  Register Got Number");}   
					| reg_reg immed immed  error {yyerror("Unexpected Input, Register Expected");}   
					| reg_reg '$' register '$' register '$' register error {yyerror("Useless Operand");}
					; 


register			: NUMBER 
					{
						$$=$1;
						if($1>15 || $1<0)
						{	
							yyerrok;
							yyerror("Nonexistent Register");
							parsing_errors++;
						}
					}
					;		


immed				: NUMBER
					{
						$$=$1;
						if($1>MAX_IMMEDIATE_VALUE || $1<MIN_IMMEDIATE_VALUE)
						{
							yyerrok;
							yyerror("Constant Value Out Of Range");
							parsing_errors++;	

						}					
					}
					;


reg_reg				: XOR {$$=$1;}
					| SUB {$$=$1;}
					| ADD {$$=$1;}
					;


exch_stmt			: EXCH '$' register '$' register
					{
						PRINT_DEBUG_INFO(("bison exch stmt\n"));
						instructions[internal_line]=new_instruction(reg_reg,$1);
						instructions[internal_line]->input.regd=$3;
						instructions[internal_line]->input.regs=$5;
						instructions[internal_line]->input.var_name=NULL;
						internal_line++;
					}	
					| EXCH '$' register variable '(' '$' register ')'
					{
						PRINT_DEBUG_INFO(("bison exch stmt\n"));
						instructions[internal_line]=new_instruction(reg_reg,$1);
						instructions[internal_line]->input.regd=$3;
						instructions[internal_line]->input.off=$7;
						instructions[internal_line]->input.var_name=$4;
						internal_line++;
					}
					| EXCH '$' register variable %prec LOWER_THAN_PAR
					{
						PRINT_DEBUG_INFO(("bison exch stmt\n"));
						instructions[internal_line]=new_instruction(reg_reg,$1);
						instructions[internal_line]->input.regd=$3;
						instructions[internal_line]->input.off=0;
						instructions[internal_line]->input.var_name=$4;
						internal_line++;
		
					}	
					| EXCH '$' register variable '(' ')' error {yyerror("Missing Array Offset");}
					| EXCH '$' register error {yyerror("Missing Operand");} 
					| EXCH error  {yyerror("Missing Operand");}
					| EXCH immed error {yyerror("Unexpected Input,Expected  Register Got Number");}   
					| EXCH immed immed  error {yyerror("Unexpected Input, Register Expected");}   
					;


reg_noth_stmt		: reg_noth  '$' register 
					{
						PRINT_DEBUG_INFO(("bison %s stmt\n",$1));
						instructions[internal_line]=new_instruction(reg_noth,$1);
						instructions[internal_line]->input.regd=$3;
						internal_line++;
					}
					| reg_noth error {yyerror("Missing Operand");} 
					| reg_noth immed error {yyerror("Unexpected Input, Register Expected");}   
					| reg_noth '$' register '$' register  error {yyerror("Unseless Operand");}   
					;
	 
reg_noth			: NEG {$$=$1;}
					| ADD1 {$$=$1;}
					| SUB1 {$$=$1;}
					;
				


xori_stmt			: XORI  '$' register  immed
					{
						PRINT_DEBUG_INFO(("bison xori stmt\n"));
						instructions[internal_line]=new_instruction(reg_imm,$1);
						instructions[internal_line]->input.regd=$3;
						instructions[internal_line]->input.imm=$4;
						internal_line++;
					}
					| XORI error {yyerror("Missing Operand");}
					| XORI '$' register error {yyerror("Missing Constant");}
					| XORI immed error {yyerror("Unexpected Input, Expected Register Got Number");}
					| XORI '$' register '$' error {yyerror("Unexpected Input, Expected Number Got Register");}
					; 

product_stmt		: product  '$' register
					{
						PRINT_DEBUG_INFO(("bison mul2 stmt\n"));
						instructions[internal_line]=new_instruction(reg_noth,$1);
						instructions[internal_line]->input.regd=$3;
						internal_line++;
					}
					| product error {yyerror("Missing Operand");} 
					| product  immed error {yyerror("Unexpected Input, Register Expected");}   
					| product '$' register '$' register  error {yyerror("Unseless Operand");}   
					;

product				: DIV2 {$$=$1;}
					| MUL2 {$$=$1;}
					;


branches_stmt		: branch '$' register
					{ 
						PRINT_DEBUG_INFO(("bison %s stmt\n",$1));
						instructions[internal_line]=new_instruction(reg_off,$1);
						instructions[internal_line]->input.regd=$3;
					} 
					label 
					{		
						internal_line++;
					}
					| branch error {yyerror("Missing Operand");}
					| branch immed error {yyerror("Unexpected Input, Register Expected Got Number");}
					| branch '$' register '$' register  error {yyerror("Unseless Operand");}   
					| branch '$' register error {yyerror("Missing Offset");};
					;


label				: '>' ID
					{
						new_inbranch($2);
					}
					| ID error {yyerror("Dangling Label");}
					| '>' error   {yyerror("Dangling Arrow");}
					| %empty {yyerror("Missing Offset");}
					;

label2				: ID ':' 
					{
						PRINT_DEBUG_INFO(("bison label %s",$1));
						new_outbranch($1);
					
					}
					| ID error {yyerror("Dangling Label");}
					| ':' error   {yyerror("Dangling Arrow");}
					;


branch				: BGEZ {$$=$1;}
					| BLZ {$$=$1;}
					| BODD {$$=$1;}
					| BEVN {$$=$1;}
					;


ubranches_stmt		: ubranch
					{	
						PRINT_DEBUG_INFO(("bison %s stmt\n",$1));
						instructions[internal_line]=new_instruction(noth_off,$1);
					}
					label
					{
						internal_line++;
					}						
					; 

ubranch				: BRA {$$=$1;}
					| RBRA {$$=$1;}
					;



swbr_stmt			: SWBR  '$' register
					{
						PRINT_DEBUG_INFO(("bison swbr stmt\n"));
						instructions[internal_line]=new_instruction(reg_off,$1);
						instructions[internal_line]->input.regd=$3;
						internal_line++;
					}
					| SWBR error {yyerror("Missing Operand");}
					| SWBR immed error {yyerror("Unexpected Input, Register Expected Got Number");}
					| SWBR '$' register '$' register  error {yyerror("Unseless Operand");}   
					;
 
%%

Instruction *new_instruction(int category,char *name)
{
	Instruction *in=(Instruction*)malloc(sizeof(Instruction));	
	if(in==NULL)
	{
		printf("Cant Allocate Memory For Instruction\n");
		exit(1);
	}
	in->category=category;
	in->instr_id=find_instr_id(name);
	in->actual_line=yylineno-1;
	in->input.label=NULL;
	in->input.regd=0;
	in->input.regs=0;
	in->input.off=0;
	in->input.imm=0;
	return in;
}


void check_data_len()
{
	//allocate more space for instructions	
	if(internal_line==(instruction_count-1))
	{
		instruction_count*=2;
		instructions=(Instruction **)realloc(instructions,sizeof(Instruction *)*instruction_count);
		if(instructions==NULL)
		{
			printf("Cant Reallocate Memory for Instructions\n");
			exit(1);
		}
	}
	if(lbl==(label_count-1))
	{
		//allocate and initialize more space for labels
		label_count*=2;
		labels=(Label**)realloc(labels,sizeof(Label *)*label_count);
		if(labels==NULL)
		{
			printf("Cant Reallocate Memory for Labels\n");
			exit(1);
		}	
	}

}

void write_labels()
{
	int j,i;
	for(i=0;i<instruction_count;i++)
	{	
		if((instructions[i]->category==reg_off || instructions[i]->category==noth_off) &&
														instructions[i]->input.label!=NULL)
		{	
			for(j=0;j<lbl;j++)
			{	
				if(strcmp(instructions[i]->input.label,labels[j]->string)==0)
				{	
					int offset=0;
					free(instructions[i]->input.label);
					offset=(labels[j]->line_number)-i;
					if(offset<MIN_OFFSET_VALUE || offset>MAX_OFFSET_VALUE)
					{
						risa_error("Offset out of limits",instructions[i]->actual_line,simulator);
						parsing_errors++;
						return;
					}
					instructions[i]->input.off=(labels[j]->line_number)-i;
					break;
				}
			}
			if(j==lbl)
			{
				risa_error("Nonexistent offset",instructions[i]->actual_line,simulator);
				parsing_errors++;
				return;	
			}	
		}
	}
	free_labels();
}

void release_tok_mem(Instruction **in)
{
	int i=0;
	if(in==NULL) return;
	for(i=0;i<internal_line;i++)
	{
		if(in[i]!=NULL)
		{	
			free(in[i]);
			in[i]=NULL;
		}
	}
	instruction_count=0;
	internal_line=0;
	free(in);
	in=NULL;

}

void free_labels()
{
	int i; 
	if(labels==NULL) return;
	for(i=0;i<lbl;i++)
	{
		if(labels[i]!=NULL)
		{
			if(labels[i]->string!=NULL)
			{	
				free(labels[i]->string);
			}
			free(labels[i]);
			labels[i]=NULL;	
		}
	}
	lbl=0;
	label_count=0;
	
	free(labels);
	labels=NULL;
}

void print_the_code()
{
	int i=0;
	Instruction **in=instructions;
	for(i=0;i<instruction_count;i++)
	{
		printf("---------------------------------\n");
		printf("Instruction id: %d, name: %s\n",in[i]->instr_id,
														instructions_strings[in[i]->instr_id]);
		printf("Category:%d \n",in[i]->category);
		if(in[i]->category==reg_reg)
			printf("Inputs regd:%d, regs: %d\n",in[i]->input.regd,in[i]->input.regs);
		else if(in[i]->category==reg_noth)
			printf("Inputs regs:%d\n",in[i]->input.regd);
		else if(in[i]->category==reg_off)
			printf("Inputs regd:%d, offset:%d\n",in[i]->input.regd,in[i]->input.off);	
		else if(in[i]->category==reg_imm)
			printf("Inputs regd:%d, imm: %d\n",in[i]->input.regd,in[i]->input.imm);
		else if(in[i]->category==noth_off)
			printf("Inputs offset: %d\n",in[i]->input.off);	
		else if (in[i]->category==stop_cat)
			printf("No inputs\n");
		printf("Actual Line: %d\n",in[i]->actual_line);
	}	
}

int find_instr_id(char* str)
{
	int i=0,id=-1;
	char s[strlen(str)+1];
	strcpy(s,str);
	for(i=0;i<INSTRUCTION_COUNT;i++)
	{
		if(strcmp(str,instructions_strings[i])==0)
		{
			id=i;
			break;
		}		
	}
	return id;
}

void new_inbranch(char* tolabel)
{
	instructions[internal_line]->input.label=strdup(tolabel);
}

void new_outbranch(char *currlabel)
{
	labels[lbl]=(Label*)malloc(sizeof(Label));	
	labels[lbl]->string=strdup(currlabel);
	labels[lbl]->line_number=internal_line;
	lbl++;	
}

void new_fullbranch(char* currlabel,char* tolabel)
{
	new_inbranch(tolabel);
	new_outbranch(currlabel);
}


void yyerror(char const *s) 
{
	yyclearin;
	parsing_errors++;
	risa_error(s,yylineno-1,simulator);
}
